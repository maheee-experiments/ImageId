package net.mahes.imageid;

public class ImageId {

	public final byte[] red;
	public final byte[] green;
	public final byte[] blue;
	public final byte[] gray;
	
	public ImageId(int length) {
		red = new byte[length];
		green = new byte[length];
		blue = new byte[length];
		gray = new byte[length];
	}

	private String byteArrayToString(byte[] arr) {
		String res = "";
		for (byte b : arr) {
			res += String.format("%02X", b);
		}
		return res;
	}

	@Override
	public String toString() {
		String res = "";
		res += byteArrayToString(gray);
		res += byteArrayToString(red);
		res += byteArrayToString(green);
		res += byteArrayToString(blue);
		return res;
	}
	
}
