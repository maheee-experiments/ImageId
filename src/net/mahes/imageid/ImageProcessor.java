package net.mahes.imageid;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinAttributes;
import net.mahes.imageid.plugins.SerializePlugin;
import net.mahes.imageid.plugins.ShrinkPlugin;

public class ImageProcessor {

	private MarvinImagePlugin shrinkPlugin = new ShrinkPlugin();
	private MarvinImagePlugin serializePlugin = new SerializePlugin();

	private MarvinImage procImage;

	public ImageProcessor(int width, int height) {
		procImage = new MarvinImage(width, height);
	}

	public ImageId process(String path) {
		MarvinImage imageIn = MarvinImageIO.loadImage(path);

		MarvinAttributes attributes = new MarvinAttributes();

		shrinkPlugin.process(imageIn, procImage);
		serializePlugin.process(procImage, null, attributes);

		return (ImageId) attributes.get("ii");
	}

	public MarvinImage getProcImage() {
		return procImage;
	}

}
