package net.mahes.imageid.plugins;

import marvin.gui.MarvinAttributesPanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractImagePlugin;
import marvin.util.MarvinAttributes;
import net.mahes.imageid.ImageId;
import net.mahes.imageid.plugins.util.Coord;
import net.mahes.imageid.plugins.util.Spiral;

public class SerializePlugin extends MarvinAbstractImagePlugin {

	@Override
	public void load() {
	}

	@Override
	public MarvinAttributesPanel getAttributesPanel() {
		return null;
	}

	@Override
	public void process(MarvinImage imageIn, MarvinImage imageOut, MarvinAttributes attrOut, MarvinImageMask mask,
			boolean previewMode) {

		int width = imageIn.getWidth();
		int height = imageIn.getHeight();
		int halfWidth = width / 2;
		int halfHeight = height / 2;
		int length = width * height;
		
		ImageId ii = new ImageId(length);

		int i = 0;
		for (Coord coord : new Spiral(width, height)) {
			int x = halfWidth + coord.x;
			int y = halfHeight + coord.y;
			
			ii.red[i] = (byte) imageIn.getIntComponent0(x, y);
			ii.green[i] = (byte) imageIn.getIntComponent1(x, y);
			ii.blue[i] = (byte) imageIn.getIntComponent2(x, y);
			ii.gray[i] = (byte) ((imageIn.getIntComponent0(x, y) + imageIn.getIntComponent1(x, y) + imageIn.getIntComponent2(x, y)) / 3);
			
			++i;
		}
		
		attrOut.set("ii", ii);
	}

}
