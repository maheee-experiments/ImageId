package net.mahes.imageid.plugins.util;

public class Coord {
	public final int x;
	public final int y;

	public Coord(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
}
