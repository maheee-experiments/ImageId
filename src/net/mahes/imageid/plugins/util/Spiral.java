package net.mahes.imageid.plugins.util;

import java.util.Iterator;

public class Spiral implements Iterable<Coord> {
	private int width, height;

	public Spiral(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public Iterator<Coord> iterator() {
		return new SpiralIterator(width, height);
	}

	class SpiralIterator implements Iterator<Coord> {
		private int x = 0, y = 0;
		private int dx = 0, dy = -1;
		private int steps = 1;

		private int width, height;
		private int maxSteps;

		private Coord next = new Coord(0, 0);

		public SpiralIterator(int width, int height) {
			this.width = width;
			this.height = height;

			int sizeMax = Math.max(width, height);
			maxSteps = sizeMax * sizeMax;
		}

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public Coord next() {
			Coord result = next;
			calcNext();
			return result;
		}

		private void calcNext() {
			if (steps >= maxSteps) {
				next = null;
				return;
			}

			if ((x == y) || ((x < 0) && (x == -y)) || ((x > 0) && (x == 1 - y))) {
				changeDir();
			}

			move();

			++steps;

			if (isInBounds()) {
				next = new Coord(x, y);
				return;
			}

			calcNext();
		}

		private void changeDir() {
			int t = dx;
			dx = -dy;
			dy = t;
		}

		private void move() {
			x += dx;
			y += dy;
		}

		private boolean isInBounds() {
			return (-width / 2 <= x) && (x <= width / 2) && (-height / 2 <= y) && (y <= height / 2);
		}

	}
}
