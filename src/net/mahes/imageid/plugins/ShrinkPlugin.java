package net.mahes.imageid.plugins;

import marvin.gui.MarvinAttributesPanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractImagePlugin;
import marvin.util.MarvinAttributes;

public class ShrinkPlugin extends MarvinAbstractImagePlugin {

	@Override
	public void load() {
	}

	@Override
	public MarvinAttributesPanel getAttributesPanel() {
		return null;
	}

	@Override
	public void process(MarvinImage imageIn, MarvinImage imageOut, MarvinAttributes attrOut, MarvinImageMask mask, boolean previewMode) {
		double xLength = (double) imageIn.getWidth() / (double) imageOut.getWidth();
		double yLength = (double) imageIn.getHeight() / (double) imageOut.getHeight();

		for (int x = 0; x < imageOut.getWidth(); ++x) {
			for (int y = 0; y < imageOut.getHeight(); ++y) {
				int xStart = (int) (x * xLength);
				int yStart = (int) (y * yLength);
				int xEnd = (int) (xStart + xLength);
				int yEnd = (int) (yStart + yLength);
				int[] colors = getAverageColor(imageIn, xStart, yStart, xEnd, yEnd);
				imageOut.setIntColor(x, y, colors[0], colors[1], colors[2]);
			}
		}
	}

	protected int[] getAverageColor(MarvinImage imageIn, int xStart, int yStart, int xEnd, int yEnd) {
		long r = 0, g = 0, b = 0;
		long c = 0;

		for (int x = xStart; x < xEnd; ++x) {
			for (int y = yStart; y < yEnd; ++y) {
				++c;
				r += imageIn.getIntComponent0(x, y);
				g += imageIn.getIntComponent1(x, y);
				b += imageIn.getIntComponent2(x, y);
			}
		}

		return new int[]{(int) (r / c), (int) (g / c), (int) (b / c)};
	}
	
}
