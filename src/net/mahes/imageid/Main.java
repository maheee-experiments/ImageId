package net.mahes.imageid;

import marvin.io.MarvinImageIO;

public class Main {

	public static void main(String[] args) {

		ImageProcessor ip = new ImageProcessor(5, 5);
		
		ImageId ii = ip.process("/Users/mhehl/Desktop/in.jpg");
		
		System.out.println(ii);

		MarvinImageIO.saveImage(ip.getProcImage(), "/Users/mhehl/Desktop/out.jpg");
	}

}
